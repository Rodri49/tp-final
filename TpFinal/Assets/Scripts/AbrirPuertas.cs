using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;

public class AbrirPuertas : MonoBehaviour
{
    public GameObject puerta;
    public Transform jugador;
    public KeyCode teclaDeApertura = KeyCode.E;
    public float distanciaDeApertura = 10.0f;
    public float velocidadDeApertura = 5.0f;

    private bool puertaAbierta = false;
    private Vector3 posicionInicial;
    public ControlJugador controlJugador;
    void Start()
    {
        posicionInicial = puerta.transform.position;
    }

    void Update()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);
        Debug.Log(controlJugador.tienellave);
        if (Input.GetKeyDown(teclaDeApertura) && distanciaAlJugador <= distanciaDeApertura && controlJugador.tienellave)
        {
            puertaAbierta = !puertaAbierta;
            AbrirCerrarPuerta();
        }
    }
    
    void AbrirCerrarPuerta()
    {
        Vector3 posicionFinal;

        if (puertaAbierta)
        {
            posicionFinal = posicionInicial + Vector3.right * 90.0f;
        }
        else
        {
            posicionFinal = posicionInicial;
        }

        puerta.transform.position = Vector3.Lerp(puerta.transform.position, posicionFinal, Time.deltaTime * velocidadDeApertura);
    }
}
