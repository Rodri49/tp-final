using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuertasFabrica : MonoBehaviour
{
    public GameObject puerta;
    public Transform jugador;
    public KeyCode teclaDeApertura = KeyCode.E;
    public float distanciaDeApertura = 10.0f;
    public float velocidadDeApertura = 2.0f;

    private bool puertaAbierta = false;
    private Vector3 posicionInicial;

    void Start()
    {
        posicionInicial = puerta.transform.position;
    }

    void Update()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);

        if (Input.GetKeyDown(teclaDeApertura) && distanciaAlJugador <= distanciaDeApertura)
        {
            puertaAbierta = !puertaAbierta;

            AbrirCerrarPuerta();
        }
    }

    void AbrirCerrarPuerta()
    {
        Vector3 posicionFinal;

        if (puertaAbierta)
        {
            posicionFinal = posicionInicial + Vector3.back * 70.0f;
        }
        else
        {
            posicionFinal = posicionInicial;
        }

        puerta.transform.position = Vector3.Lerp(puerta.transform.position, posicionFinal, Time.deltaTime * velocidadDeApertura);
    }
}
