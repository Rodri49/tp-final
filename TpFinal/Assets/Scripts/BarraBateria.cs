using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraBateria : MonoBehaviour
{
    public float Bateria = 100;
    public float BateriaMaxima = 100;
    public float velocidadDescenso = 1.0f;
    public GameObject SpotLight;

    public Image BarraBat;

    void Update()
    {
        Bateria -= velocidadDescenso * Time.deltaTime;
        Bateria = Mathf.Clamp(Bateria, 0f, BateriaMaxima);
        ActualizarInterfaz();
        if (Bateria <= 0)
        {
            ApagarLinterna();
        }
    }

    void ActualizarInterfaz()
    {
        BarraBat.fillAmount = Bateria / BateriaMaxima;
    }

    public void RecargarLinterna()
    {
        Bateria = Bateria +20;
        EncenderLinterna();
    }

    void EncenderLinterna()
    {
        SpotLight.SetActive(true);
    }

    void ApagarLinterna()
    {
        SpotLight.SetActive(false);
    }
}
