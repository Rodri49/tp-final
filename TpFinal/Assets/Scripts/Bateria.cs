using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bateria : MonoBehaviour
{
    public float cantidadRecarga = 20f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Linterna"))
        {
            BarraBateria barraBateria = other.GetComponent<BarraBateria>();

            if (barraBateria != null)
            {
                barraBateria.Bateria += cantidadRecarga;
                barraBateria.Bateria = Mathf.Clamp(barraBateria.Bateria, 0f, barraBateria.BateriaMaxima);

                
                if (barraBateria.Bateria > 0 && !barraBateria.SpotLight.activeSelf)
                {
                    barraBateria.RecargarLinterna();
                }

                Destroy(gameObject);
            }
        }
    }
}
