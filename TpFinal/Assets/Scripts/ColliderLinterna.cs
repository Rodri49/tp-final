using UnityEngine;

public class ColliderLinterna : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Fantasma"))
        {
            DetenerEnemigo(other.gameObject);
        }
    }

    void DetenerEnemigo(GameObject enemigo)
    {
        PerseguirJugador scriptEnemigo = enemigo.GetComponent<PerseguirJugador>();
        if (scriptEnemigo != null)
        {
            scriptEnemigo.DetenerEnemigo();
        }
    }
}
