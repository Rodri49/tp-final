using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public float velocidadAumentada = 8.0f;
    private bool aumentarVelocidad = false;
    public bool tienellave = false;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    void Update()
    {
        aumentarVelocidad = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        float velocidad;
        if (aumentarVelocidad)
        {
            velocidad = velocidadAumentada;
        }
        else
        {
            velocidad = rapidezDesplazamiento;
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * velocidad;
        float movimientoCostados = Input.GetAxis("Horizontal") * velocidad;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
 
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Llave"))
        {
            tienellave = true;
            Debug.Log(tienellave);
            Destroy(other.gameObject);
        }

        if (other.CompareTag("Puerta") && tienellave)
        {
            Debug.Log("�Has abierto la puerta! �Ganaste!");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Pasto"))
        {
            StartCoroutine(Caminata(3));
        }
    }

    private IEnumerator Caminata(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(0.5f);
            tiempo -= 0.5f;
        }
        GestorDeAudio.instancia.ReproducirSonido("Caminar");
    }
     
}
