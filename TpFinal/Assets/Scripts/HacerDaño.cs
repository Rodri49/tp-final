using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HacerDaño : MonoBehaviour
{
    public float CantidadDaño;

    private void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.CompareTag("Jugador") && other.gameObject.GetComponent<SaludJugador>())
        {
            other.gameObject.GetComponent<SaludJugador>().RecibirDaño(CantidadDaño);
        }
    }
    
}
