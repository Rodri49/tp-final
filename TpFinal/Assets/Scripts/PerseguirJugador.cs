using UnityEngine;

public class PerseguirJugador : MonoBehaviour
{
    private GameObject jugador;
    public float rapidez;
    public float VelocidadInicial;
    private bool linternaDetectada = false;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }
    private void Update()
    {
        if (linternaDetectada)
        {
            DetenerEnemigo();
        }
        else
        {
            if (!linternaDetectada)
            {
                ReiniciarEnemigo();
            }

            transform.LookAt(jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Spotlight"))
        {
            linternaDetectada = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Spotlight"))
        {
            linternaDetectada = false;
        }
    }
    public void DetenerEnemigo()
    {
        rapidez = 0f;
    }
    public void ReiniciarEnemigo()
    {
        rapidez = VelocidadInicial;
    }
}
