using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class SaludJugador : MonoBehaviour
{
    public float Salud = 100;
    public float SaludMaxima = 100;
    
    [Header ("Interfaz")]
    public Image BarraSalud;
    public Text TextoSalud;

    [Header("Muerto")]
    public GameObject Muerto;

    void Update()
    {
        ActualizarInterfaz();
    }

    
    void ActualizarInterfaz()
    {
        BarraSalud.fillAmount = Salud / SaludMaxima;
        TextoSalud.text = "Psiquis:" + Salud.ToString(); 
    }
   
    public void RecibirDa�o(float da�o)
    {
        Salud -= da�o;
        Debug.Log("aaaaa");
        if (Salud <= 0)
        {
            Muerto.SetActive(true);
        }
    }
}
